/*
 * ConnectNative.cpp
 *
 *  Created on: 2014/06/06
 *      Author: nogami
 */
#include "ConnectNative.h"
#include <jni.h>
#include <stdio.h>
#include <stdlib.h>
#include "platform/android/jni/JniHelper.h"

#define  CLASS_NAME "org/cocos2dx/cpp/AppActivity"
using namespace cocos2d;

namespace native_plugin
{
    std::string ConnectNative::getUniqueIdentifier(){
        JniMethodInfo minfo;
        CCAssert(JniHelper::getStaticMethodInfo(minfo, CLASS_NAME, "getUniqueIdentifier", "()Ljava/lang/String;"), "Function doesn't exist");
        
        jstring name = (jstring) minfo.env->CallStaticObjectMethod(minfo.classID, minfo.methodID);
        minfo.env->DeleteLocalRef(minfo.classID);
        
        const char *nativeString = minfo.env->GetStringUTFChars(name, 0);
        CCLOG("Getting Unique identifier : %s", nativeString);
        std::string identifier = std::string(nativeString);
        minfo.env->ReleaseStringUTFChars(name, nativeString);
        return identifier;
    }
    
    std::string ConnectNative::getDeviceModel(){
        JniMethodInfo minfo;
        CCAssert(JniHelper::getStaticMethodInfo(minfo, CLASS_NAME, "getDeviceModel", "()Ljava/lang/String;"), "Function doesn't exist");
        
        jstring name = (jstring) minfo.env->CallStaticObjectMethod(minfo.classID, minfo.methodID);
        minfo.env->DeleteLocalRef(minfo.classID);
        
        const char *nativeString = minfo.env->GetStringUTFChars(name, 0);
        CCLOG("Getting Device Model : %s", nativeString);
        std::string identifier = std::string(nativeString);
        minfo.env->ReleaseStringUTFChars(name, nativeString);
        return identifier;
    }
}



